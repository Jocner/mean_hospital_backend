require('dotenv').config();

const express = require('express');
const cors = require('cors');

const dbConnection = require('./database/config');

//Crear el servidor express
const app = express();

//Configurar CORS
app.use(cors());

//Base de Dato
dbConnection();


app.listen( process.env.PORT, () => {
    console.log('Servidor corriendo en puerto ' + process.env.PORT );
});
